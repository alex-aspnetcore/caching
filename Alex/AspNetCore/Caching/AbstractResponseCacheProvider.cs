using Microsoft.AspNetCore.Http;

namespace Alex.AspNetCore.Caching
{
    public abstract class AbstractResponseCacheProvider : CacheProviderInterface
    {
        public abstract ICacheOutputItem GetCachedItem(string key);
        public abstract void CacheItem(string key, ICacheInputItem item);
        public abstract bool IsCachedEntryValid(ICacheOutputItem outputItem);
        public void MarkForCaching(HttpContext context)
        {
            context.Items.Add(NeedsCacheKey, this);
        }
        
        internal virtual string GetCacheKey(HttpContext context)
        {
            string path = context.Request.Path;
            string query = context.Request.QueryString.Value;
            return path + query;
        }
    }
}